---
title: ts类型
date: 2022-02-15 13:48:15
tags: ts类型
category: ts类型
---
- 类型：

  |  类型   |       例子        |              描述              |
  | :-----: | :---------------: | :----------------------------: |
  | number  |    let name:number = 1, -33, 2.5    |            任意数字            |
  | string  | let name:string = "bob"; |           任意字符串 ，可以使用模版字符串镶嵌           |
  | boolean | let isDone:boolean = false;    |       布尔值true或false        |
  |  array  |     string[] numder[] any[]......    |           任意JS数组           |
  |  tuple  |     let x: [string, number]; ['hello', 10]      | 元组类型允许表示一个已知元素数量和类型的数组，各元素的类型不必相同，增加和减少会报错，固定长度和类型 |
  |  enum   |    enum Color {Red, Green, Blue};let c: Color = Color.Green;     |      使用枚举类型可以为一组数值赋予友好的名字       |
  |   any   |         *        |            任意类型            |
  | 字面量  |      其本身       |  限制变量的值就是该字面量的值  |

  | unknown |         *         |         类型安全的any          |
  |  void   | 空值（undefined） |     没有值（或undefined）      |
  |  never  |      没有值       |          不能是任何值          |
  | object  |  {name:'孙悟空'}  |          任意的JS对象          |


- number 数字
  - ```typescript
    let decimal: number = 6;
    let hex: number = 0xf00d;
    let binary: number = 0b1010;
    let octal: number = 0o744;
    let big: bigint = 100n;
    ```

- boolean 布尔值
  - ```typescript
    let isDone: boolean = false;
    ```

- string 字符串

  - ```typescript
    let color: string = "blue";
    color = 'red';
    let fullName: string = `Bob Bobbington`;
    let age: number = 37;
    // 模板字符串整合
    let sentence: string = `Hello, my name is ${fullName}.
    I'll be ${age + 1} years old next month.`;
    ```

- 字面量
  - 也可以使用字面量去指定变量的类型，通过字面量可以确定变量的取值范围，
    规范这个变量的取值范围
  - ```typescript
    let color: 'red' | 'blue' | 'black';
    let num: 1 | 2 | 3 | 4 | 5;
    color = 'yellow'// 报错
    color = 'red'// 正确
    ```

- any 全类型
  - ```typescript
    let d: any = 4;
    d = 'hello';
    d = true;
    ```
    
- unknown

  - ```typescript
    let notSure: unknown = 4;
    notSure = 'hello';
    ```

- void
 - 它表示没有任何类型。 当一个函数没有返回值时，你通常会见到其返回值类型是 void
  - ```typescript
    let unusable: void = undefined;
    ```

- never
  - never类型表示的是那些永不存在的值的类型
  - ```typescript
    function error(message: string): never {
      throw new Error(message);
    }
    ```

- object（没啥用）

  - ```typescript
    let obj: object = {};
    ```

- array 数组

  - ```typescript
    let list0: number[] = [1, 2, 3];// 数字类型数组 写法1
    let list1: Array<String> = ['1', '2', '3'];// 字符串类型数组 写法2
    let list2: any[] = [1, '2', {name:'12'},true,null];// 全类型数组
    ```

- tuple 元组 Tuple
  - 元组类型允许表示一个已知元素数量和类型的数组，各元素的类型不必相同，增加和减少会报错，固定长度和类型
  - ```typescript
    let x: [string, number];
    x = ["hello", 10]; 
    console.log(x[1].substr(1)); // Error, 'number' does not have 'substr'
    ```

- enum 枚举

  - ```typescript
    // 自定义的
    enum Color {
      Red = '#red',
      Green= '#Green',
      Blue= '#Blue',
    }
    let c: Color = Color.Green; // #Green
    
    // 默认的
    enum Color {
      Red = 1,
      Green = 2,
      Blue = 3,
    }
    let c: Color = Color[2];// c = Green
    ```

- 类型断言

  - 有些情况下，变量的类型对于我们来说是很明确，但是TS编译器却并不清楚，此时，可以通过类型断言来告诉编译器变量的类型，断言有两种形式：

    - 第一种 as

      - ```typescript
        let someValue: unknown = "this is a string";
        let strLength: number = (someValue as string).length;// as string判断的上面的类型
        ```

    - 第二种 <string>

      - ```typescript
        let someValue: unknown = "this is a string";
        let strLength: number = (<string>someValue).length;// <string>判断的上面的类型
        ```

        
